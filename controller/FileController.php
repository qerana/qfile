<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\controller;

use Qerapp\qfile\model\file\FileService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  File
  |*****************************************************************************
  |
  | Controller File
  | @author qDevTools,
  | @date 2020-06-19 21:49:05,
  |*****************************************************************************
 */

class FileController extends \Qerana\core\QeranaC {

    protected
            $_FileService;

    public function __construct() {
        parent::__construct();
        $this->_FileService = new FileService;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson($id_category = false) {
        
        $condition = ($id_category) ? ['id_category' => $id_category] : [];
        
        return $this->_FileService->getAll(true,$condition);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id) {
        return $this->_FileService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void {


        $vars = [
            'Plugins' => [
                'data_json.js',
                'app/qfile/file.js'
            ]
        ];
        \Qerana\core\View::showView('file/index_file', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(int $id_category = 0): void {

        
        
        $vars = [
            'Categories' => $this->_FileService->getCategories(),
            'id_category' => filter_var($id_category,FILTER_SANITIZE_NUMBER_INT)
        ];

        
        \Qerana\core\View::showForm('file/partials/add_file', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void {

        $this->_FileService->upload();
        \helpers\Redirect::toAction('index');
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void {


        $File = $this->_FileService->getById($id);
        
        $File->setDownloads();
        $vars = [
            'id_file' => $id,
            'File' => $File,
            'Plugins' => [
                'data_json.js',
                'app/qfile/file.js'
            ]
        ];

        \Qerana\core\View::showView('file/detail_file', $vars);
    }

     /**
     * -------------------------------------------------------------------------
     * View file property
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function property(int $id): void {


        $File = $this->_FileService->getById($id);
        
        $File->setDownloads();
        $vars = [
            'id_file' => $id,
            'File' => $File,
            'Plugins' => [
                'data_json.js',
                'app/qfile/file.js'
            ]
        ];

        \Qerana\core\View::showView('file/partials/file_property', $vars);
    }
    
    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void {

        $vars = [
            'File' => $this->_FileService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qfile/file.js'
            ]
        ];
        \Qerana\core\View::showForm('file/edit_file', $vars);
    }

    /**
     *  Download a file
     * @param int $id
     * @return type
     */
    public function download(int $id) {

        return $this->_FileService->download($id);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void {
        $this->_FileService->saveChanges();
        \helpers\Redirect::toAction('detail/'.$this->_FileService->File->id_file);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id_category,int $id_file): void {
        $this->_FileService->delete($id_file);
        \helpers\Redirect::to('/qfile/category/index/'.$id_category);
    }

}
