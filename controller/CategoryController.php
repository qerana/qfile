<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\controller;

use Qerapp\qfile\model\file\CategoryService,
    Qerapp\qfile\model\file\BreadCrumbService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Category
  |*****************************************************************************
  |
  | Controller Category
  | @author qDevTools,
  | @date 2020-06-19 21:48:44,
  |*****************************************************************************
 */

class CategoryController extends \Qerana\core\QeranaC {

    protected
            $_CategoryService;

    public function __construct() {
        parent::__construct();
        $this->_CategoryService = new CategoryService;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson() {
        return $this->_CategoryService->getAll(true);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id) {
        return $this->_CategoryService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(int $parent_category = 0): void {

        // if category has parent, got to folder
        if ($parent_category !== 0) {
            \helpers\Redirect::toAction('detail/' . $parent_category);
        }

        $vars = [
            'Categories' => $this->_CategoryService->getAllTop(),
            'Plugins' => [
                'data_json.js',
                'app/qfile/category.js'
            ]
        ];
        \Qerana\core\View::showView('category/index_category', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(int $parent_category = 0): void {

        $vars = [
            'parent_category' => $parent_category
        ];

        \Qerana\core\View::showForm('category/partials/add_category', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void {

        $this->_CategoryService->save();
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void {


        $vars = [
            'id_category' => $id,
            'Category' => $this->_CategoryService->getById($id),
            'Plugins' => [
                'data_json.js',
                'jquery.form.js',
                'app/qfile/category.js'
            ]
        ];

        \Qerana\core\View::showView('category/detail_category', $vars);
    }

    /**
     * Load folder view
     * @param int $id_category
     */
    public function loadFolder(int $id_category) {

        $Category = $this->_CategoryService->getById($id_category);
        $BreadCrumService = new BreadCrumbService($Category);
        $Breads  = $BreadCrumService->getBreads();
        
        $vars = [
            'id_category' => $Category->id_category,
            'Category' => $Category,
            'Breads' => $Breads,
            'Plugins' => [
                'data_json.js',
                'jquery.form.js',
                'app/qfile/category.js'
            ]
        ];

        \Qerana\core\View::showForm('category/partials/folder', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function loadCategory(int $id): void {


        $vars = [
            'id_category' => $id,
            'Category' => $this->_CategoryService->getById($id),
            'Plugins' => [
                'data_json.js',
                'jquery.form.js',
                'app/qfile/category.js'
            ]
        ];

        \Qerana\core\View::showView('category/category_files', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void {

        $vars = [
            'Category' => $this->_CategoryService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/qfile/category.js'
            ]
        ];
        \Qerana\core\View::showForm('category/edit_category', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void {
        $this->_CategoryService->save();
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void {
        $id_category = $this->_CategoryService->delete($id);
        \helpers\Redirect::toAction('index/' . $id_category);
    }

}
