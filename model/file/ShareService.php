<?php

/*
 * Copyright (C) 2020-2021 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file;

use Qerapp\qfile\model\file\entity\ShareEntity,
    Qerapp\qfile\model\file\mapper\ShareMapper,
    Qerapp\qfile\model\file\interfaces\ShareMapperInterface,
    Qerapp\qfile\model\file\repository\ShareRepository,
    Qerapp\qaccess\model\user\UserService;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity ShareService
  | @author qDevTools,
  | @date 2020-10-27 04:22:50,
  |*****************************************************************************
 */

class ShareService {

    public
            $id_file,
            //RELATED-MAPPER-OBJECT
            $ShareRepository,
            /** @object entity Share */
            $Share;

    public function __construct(ShareMapperInterface $Mapper = null) {

        //RELATED-MAPPER-OBJECT-NEW



        try {
            $ShareMapper = new ShareMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.ShareService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $ShareMapper : $Mapper;
        $this->ShareRepository = new ShareRepository($MapperRepository);
    }

    /**
     * -------------------------------------------------------------------------
     * simple test
     * -------------------------------------------------------------------------
     */
    public function shareFileWithProfiles($id_file_to_share = '') {

        // array de profiles
        $profiles_array = filter_input(INPUT_POST, 'profiles', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $this->id_file = \helpers\Request::getValue('f_id_file', $id_file_to_share);


        foreach ($profiles_array AS $id_profile):

            // get users profile
            $UserService = new UserService;
            $Users = $UserService->getUsersByProfile($id_profile);

            // share with each user
            foreach ($Users AS $User):

                $data = [
                    'id_file' => $this->id_file,
                    'id_user' => $User->id_user
                ];
                // check if exists
                $ShareCheck = new ShareRepository(new ShareMapper);
                $Exists = $ShareCheck->find($data);

                // if not exists
                if (!$Exists) {
                    $ShareRepo = new ShareRepository(new ShareMapper());
                    $this->createShare($data);
                    $ShareRepo->store($this->Share);
                }


            endforeach;


        endforeach;
    }

    /**
     * Get all shares file
     * @param int $id_file
     * @return type
     */
    public function getSharesFile(int $id_file) {
        return $this->ShareRepository->findById_file($id_file);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->ShareRepository->findAll();

        foreach ($Collection AS $Share):
            $Share->setFile();
        endforeach;

        if ($json) {
            echo json_encode($Collection);
        } else {
            return $Collection;
        }
    }

    /**
     *  Get all files shared with Session user
     */
    public function getSharesUser(int $id_user) {
        $MyShares = $this->ShareRepository->findById_user($id_user);
        $Files = [];
        foreach ($MyShares AS $Share):
            $Share->setFile();
            array_push($Files, $Share->File);
        endforeach;

        return $Files;
    }

    public function getSharesCategory(int $id_category) {
        $MyShares = $this->ShareRepository->findById_category($id_category);
        $Files = [];
        foreach ($MyShares AS $Share):
            $Share->setFile();
            array_push($Files, $Share->File);
        endforeach;

        return $Files;
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $this->Share = $this->ShareRepository->findById($id);
        if ($this->Share) {
            if ($json) {
                echo json_encode($this->Share);
            } else {
                return $this->Share;
            }
        } else {
            \QException\Exceptions::showHttpStatus(404, 'Share ' . $id . ' NOT FOUND!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save  array data
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $this->createShare($data);
        $this->storeShare($this->Share);
    }

    /**
     * -------------------------------------------------------------------------
     * Save  Object
     * -------------------------------------------------------------------------
     */
    public function storeShare($Share) {

        $this->ShareRepository->store($Share);
    }

    public function updateShare($Share) {

        // first check if exists
        $ShareRepo = new ShareRepository(new ShareMapper());
        $Exists = $ShareRepo->find(['id_user' => $Share->id_user, 'id_category' => $Share->id_category]);
        if (!$Exists) {
            $ShareRepoUpdate = new ShareRepository(new ShareMapper());
            $ShareRepoUpdate->modify($Share, ['id_file' => $Share->id_file, 'id_user' => $Share->id_user]);
        }
    }

    /**
     *  Create empleado entity
     * @param array $data
     */
    public function createShare(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;
        $this->Share = new ShareEntity($data_to_save);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id) {
        return $this->ShareRepository->remove($id);
    }

    /**
     * Un share a file
     * @param int $id_file
     * @return type
     */
    public function unshareFile(int $id_file) {
        return $this->ShareRepository->unshareFile($id_file);
    }

}
