<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\interfaces;
/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-06-19 21:50:03,
  |*****************************************************************************
 */

interface DownloadInterface
{

    
//SETTERS

  public function set_id_download(int  $id_download):void; 
 public function set_id_file(int  $id_file):void; 
 public function set_user(string $user):void; 
 public function set_ip_address(string $ip_address):void; 
 public function set_date_download(string $date_download):void; 
 public function set_agent(string $agent):void; 

    
//GETTERS

  public function get_id_download();
 public function get_id_file();
 public function get_user();
 public function get_ip_address();
 public function get_date_download();
 public function get_agent();

 
 
 
}