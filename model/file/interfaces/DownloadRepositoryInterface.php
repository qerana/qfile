<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\interfaces;
use  Qerapp\qfile\model\file\interfaces\DownloadInterface;
/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-06-19 21:50:03,
  |*****************************************************************************
 */

interface DownloadRepositoryInterface 
{
    
    public function findAll(array $conditions = [],array $options = []);
    
     public function findById_download(int  $id_download,array $options = []);
 public function findById_file(int  $id_file,array $options = []);
 public function findByUser(string $user,array $options = []);
 public function findByIp_address(string $ip_address,array $options = []);
 public function findByDate_download(string $date_download,array $options = []);
 public function findByAgent(string $agent,array $options = []);

    
    public function store(DownloadInterface $User);
    
    public function remove($id);
 
 
}