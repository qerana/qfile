<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\interfaces;
use  Qerapp\qfile\model\file\interfaces\CategoryInterface;
/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-06-19 21:42:00,
  |*****************************************************************************
 */

interface CategoryRepositoryInterface 
{
    
    public function findAll(array $conditions = [],array $options = []);
    
     public function findById_category(int  $id_category,array $options = []);
 public function findByCategory(string $category,array $options = []);
 public function findByAttr(string $attr,array $options = []);

    
    public function store(CategoryInterface $User);
    
    public function remove($id);
 
 
}