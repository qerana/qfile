<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\interfaces;

/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-06-19 21:43:20,
  |*****************************************************************************
 */

interface FileInterface {

//SETTERS

    public function set_id_file(int $id_file): void;

    public function set_id_category(int $id_category): void;

    public function set_id_user(string $id_user): void;

    public function set_title(string $title): void;

    public function set_file_name(string $file_name): void;

    public function set_path(string $path): void;

    public function set_date_upload(string $date_upload): void;

    public function set_description(string $description): void;

//GETTERS

    public function get_id_file();

    public function get_id_category();

    public function get_id_user();

    public function get_title();

    public function get_file_name();

    public function get_path();

    public function get_date_upload();
    
    public function get_description();
}
