<?php

/*
 * Copyright (C) 2020-2021 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\interfaces;

use Qerapp\qfile\model\file\interfaces\ShareInterface;

/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-10-27 04:22:50,
  |*****************************************************************************
 */

interface ShareRepositoryInterface {

    public function find(array $condition = [], array $options = []);

    public function findAll(array $conditions = [], array $options = []);

    public function findById_file(int $id_file, array $options = []);

    public function findById_user(int $id_user, array $options = []);

    public function findById_category(int $id_category, array $options = []);

    public function findByShare_date(string $share_date, array $options = []);

    public function store(ShareInterface $User);

    public function unshareFile(int $id_file);

    public function remove(int $id_file, int $id_user);
}
