<?php

/*
 * Copyright (C) 2020-2021 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\interfaces;

use Qerapp\qfile\model\file\interfaces\ShareInterface;

/*
  |*****************************************************************************
  | ShareMapperMapperInterface
  |*****************************************************************************
  |
  | MAPPER INTERFACE ShareMapper
  | @author qDevTools,
  | @date 2020-10-27 04:22:50,
  |*****************************************************************************
 */

interface ShareMapperInterface {

    public function findById(int $id);

    public function findAll(array $conditions = [], array $options = []);

    public function findOne(array $conditions);

    public function save(ShareInterface $User);

    public function deleteShare(int $id_file, int $id_user);

    public function deleteFileShared(int $id_file);
}
