<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\interfaces;
use  Qerapp\qfile\model\file\interfaces\FileInterface;
/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-06-19 21:43:20,
  |*****************************************************************************
 */

interface FileRepositoryInterface 
{
    
    public function findAll(array $conditions = [],array $options = []);
    
     public function findById_file(int  $id_file,array $options = []);
 public function findById_category(int  $id_category,array $options = []);
 public function findById_user(string $id_user,array $options = []);
 public function findByTitle(string $title,array $options = []);
 public function findByFile_name(string $file_name,array $options = []);
 public function findByPath(string $path,array $options = []);
 public function findByDate_upload(string $date_upload,array $options = []);
 public function findByUser_upload(string $user_upload,array $options = []);
 public function findByDesc(string $desc,array $options = []);

    
    public function store(FileInterface $User);
    
    public function remove($id);
 
 
}