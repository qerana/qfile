<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file;

use Qerapp\qfile\model\file\entity\CategoryEntity,
    Qerapp\qfile\model\file\mapper\CategoryMapper,
    Qerapp\qfile\model\file\interfaces\CategoryMapperInterface,
    Qerapp\qfile\model\file\repository\CategoryRepository;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity CategoryService
  | @author qDevTools,
  | @date 2020-06-19 21:42:00,
  |*****************************************************************************
 */

class CategoryService {

    public
    //RELATED-MAPPER-OBJECT
            $CategoryRepository,
            /** @object entity Category */
            $Category;

    public function __construct(CategoryMapperInterface $Mapper = null) {

        //RELATED-MAPPER-OBJECT-NEW



        try {
            $CategoryMapper = new CategoryMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.CategoryService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $CategoryMapper : $Mapper;
        $this->CategoryRepository = new CategoryRepository($MapperRepository);
    }

    /**
     * -------------------------------------------------------------------------
     * simple test
     * -------------------------------------------------------------------------
     */
    public function runTest() {


        echo '<h1>Test</h1>';
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->CategoryRepository->findAll();
        if ($json) {
            echo json_encode($Collection);
        } else {
            return $Collection;
        }
    }

    /**
     *  retreieve all top categories
     */
    public function getAllTop() {

        return $this->CategoryRepository->findBy_parent_category('0');
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $Category = $this->CategoryRepository->findById($id);

        if ($Category) {


            $Category->setFiles();
            $Category->setSubCategories();

            if ($json) {
                echo json_encode($Category);
            } else {
                return $Category;
            }
        } 
    }

    /**
     * -------------------------------------------------------------------------
     * Save  array data
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;

        $this->Category = new CategoryEntity($data_to_save);
        $this->saveCategory($this->Category);
    }

    /**
     * -------------------------------------------------------------------------
     * Save  Object
     * -------------------------------------------------------------------------
     */
    public function saveCategory($Category) {

        $this->CategoryRepository->store($Category);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete  a category
     * -------------------------------------------------------------------------
     * @param int $id
     * @return  int or 0, the parent_category directory
     */
    public function delete(int $id) {

        // first get category
        $this->Category = $this->getById($id);

        if ($this->Category->Files) {
            \QException\Exceptions::showError('Qfile.Delete', 'Folder is not empty!!');
        }

        if ($this->CategoryRepository->remove($this->Category)) {
            return $this->Category->parent_category;
        }
    }

}
