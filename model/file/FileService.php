<?php

/*
 * Copyright (C) 2020-21 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file;

use Qerapp\qfile\model\file\entity\FileEntity,
    Qerapp\qfile\model\file\mapper\FileMapper,
    Qerapp\qfile\model\file\interfaces\FileMapperInterface,
    Qerapp\qfile\model\file\repository\FileRepository,
    Qerapp\qfile\model\file\CategoryService,
    Qerapp\qfile\model\file\FileHandler,
    Qerapp\qfile\model\file\ShareService;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity FileService
  | @author qDevTools,
  | @date 2020-06-19 21:43:20,
  |*****************************************************************************
 */

class FileService
{

         
    public

    // si es imagen, compruieba el tipo de imagen
            $is_image = false,
            $ShareService,
            //RELATED-MAPPER-OBJECT
            $FileRepository,
            /** @object entity File */
            $File;

    public function __construct(FileMapperInterface $Mapper = null)
    {

        try {
            $FileMapper = new FileMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.FileService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $FileMapper : $Mapper;
        $this->FileRepository = new FileRepository($MapperRepository);
        $this->ShareService = new ShareService;
    }

    /**
     *  Get categoriies
     * @return type
     */
    public function getCategories()
    {

        $CategoryService = new CategoryService;
        return $CategoryService->getAll();
    }

    /**
     * Get files category, files folder behavior
     * @param int $id_category
     * @param type $json
     * @return type
     */
    public function getFilesCategory(int $id_category, $json = false)
    {

        // get category
        $CategoryService = new CategoryService();
        $Category = $CategoryService->getById($id_category);
        $FilesCategory = $Category->Files;

        // if is top category, get shared files
        if ($Category->parent_category === '0') {

            $ShareService = new ShareService;
            $Shares = $ShareService->getSharesCategory($id_category);
            $Collection = array_merge($FilesCategory, $Shares);
        } else {
            $Collection = $FilesCategory;
            foreach ($Collection AS $File):
                $File->setFileInfo();
            endforeach;
        }


        // set share contorl
        foreach ($Collection AS $File):
            $File->setShare();
            $File->setUser();
        endforeach;


        if ($json) {
            echo json_encode($Collection);
        } else {


            return $Collection;
        }
    }

    /**
     *  count all files
     * @return type
     */
    public function countAllFiles()
    {
        $Files = $this->FileRepository->findAll();
        return count($Files);
    }

    /**
     * Get last files uploads
     * @param int $limit
     */
    public function getLastUploads(int $limit = 10)
    {

        $Files = $this->FileRepository->findAll([], ['limit' => $limit, 'orderby' => 'date_upload DESC']);

        foreach ($Files AS $File):

            $File->setUser();
            $File->checkDownload();
            $File->setCategory();

        endforeach;

        return $Files;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false, array $conditions = [])
    {

        $Collection = $this->FileRepository->findAll($conditions);
        foreach ($Collection AS $File):
            $File->setCategory();
        endforeach;


        if ($json) {
            echo json_encode($Collection);
        } else {


            return $Collection;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false)
    {
        $FileRepo = new FileRepository(new FileMapper());
        $Entity = $FileRepo->findById($id);
        $Entity->setCategory();
        $Entity->setUser();
        $Entity->setShare();
        $Entity->setFileInfo();
        if ($Entity) {
            if ($json) {
                echo json_encode($Entity);
            } else {
                return $Entity;
            }
        } else {
            \QException\Exceptions::showHttpStatus(404, 'File ' . $id . ' NOT FOUND!!');
        }
    }

    /**
     * Download a file
     * @param int $id
     * @return type
     */
    public function download(int $id)
    {

        $this->File = $this->getById($id);
        $FileHandler = new FileHandler($this->File);
        return $FileHandler->downloadFile();
    }

    /**
     * -------------------------------------------------------------------------
     * Upload file
     * @param array $data , datos para subir
     * @patam bool $is_image , si queremos subir una imagen, comprobara su mime type
     * -------------------------------------------------------------------------
     */
    public function upload(array $data = [],bool $is_image = false)
    {


        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;

        $this->File = new FileEntity($data_to_save);
        $this->File->date_upload = date('Y-m-d h:i:s');
        $this->File->id_user = $_SESSION['Q_id_user'];
        // create path and upload
        $FileHandler = new FileHandler($this->File);
        // es una imagen?
        $FileHandler->is_image = $is_image;
        $FileHandler->set_path_file(date('Y') . '/');
        $FileHandler->uploadFormFile();
        $this->saveFile($this->File);
    }

    /**
     *  Save file changes
     * @param array $data
     */
    public function saveChanges(array $data = [])
    {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;

        $this->File = new FileEntity($data_to_save);
        $this->saveFile($this->File);
    }

    /**
     * -------------------------------------------------------------------------
     * Save  Object
     * -------------------------------------------------------------------------
     */
    public function saveFile($File)
    {

        $this->FileRepository->store($File);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id)
    {

        // get file
        $this->File = $this->getById($id);

        // remove from disk
        $FileHandler = new FileHandler($this->File);
        $FileHandler->delete();

        return $this->FileRepository->remove($this->File);
    }

}
