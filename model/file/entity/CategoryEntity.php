<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\entity;

use \Ada\EntityManager,
    Qerapp\qfile\model\file\interfaces\CategoryInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Category
  |*****************************************************************************
  |
  | Entity Category
  | @author qDevTools,
  | @date 2020-06-19 21:42:00,
  |*****************************************************************************
 */

class CategoryEntity extends EntityManager implements CategoryInterface, \JsonSerializable {

    public
    /** @array object , files entity */
            $Files = [],
            $SubCategories = [],
            
            /**  parents category*/
            $Parents = [];
    
    protected

    //ATRIBUTES        

    /** @var int(11), $parent_category  */
            $_parent_category,
            /** @var int(11), $id_category  */
            $_id_category,
            /** @var varchar(200), $category  */
            $_category,
            /** @var text(), $attr  */
            $_attr;

    //RELATED

    public function __construct(array $data = []) {
        $this->populate($data);
    }

//SETTERS

    public function setSubCategories(){
        $this->SubCategories = \Ada\EntityRelation::hasMany($this, 'category', 'id_category','findBy_parent_category');
    }
    
    /**
     * ------------------------------------------------------------------------- 
     * Setter for parent_category
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_parent_category($parent_category = 0): void {
        $this->_parent_category = filter_var($parent_category, FILTER_SANITIZE_STRING);
    }

    
    
    
    /**
     *  1:n with file entity
     */
    public function setFiles() {
        $this->Files = \Ada\EntityRelation::hasMany($this, 'file', 'id_category');
        
        
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_category
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_category($id_category = 0): void {
        $this->_id_category = filter_var($id_category, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for category
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_category($category = ""): void {
        $this->_category = filter_var($category, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for attr
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_attr($attr = ""): void {
        $this->_attr = filter_var($attr, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for parent_category
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_parent_category() {
        return $this->_parent_category;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_category
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_category() {
        return $this->_id_category;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for category
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_category() {
        return filter_var($this->_category, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for attr
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_attr() {
        return filter_var($this->_attr, FILTER_SANITIZE_STRING);
    }

}
