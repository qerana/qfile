<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\entity;

use \Ada\EntityManager,
    Qerapp\qfile\model\file\interfaces\ShareInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Share
  |*****************************************************************************
  |
  | Entity Share
  | @author qDevTools,
  | @date 2020-10-27 04:22:50,
  |*****************************************************************************
 */

class ShareEntity extends EntityManager implements ShareInterface, \JsonSerializable {

    public
            $File,
            $User;
    protected

    //ATRIBUTES        

    /** @var int(11), $id_category  */
            $_id_category,
            /** @var int(11), $id_file  */
            $_id_file,
            /** @var int(11), $id_user  */
            $_id_user,
            /** @var datetime(), $share_date  */
            $_share_date;

    //RELATED

    public function __construct(array $data = []) {
        $this->populate($data);

        //RELATED-LOAD
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_category
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_category($id_category = 0): void {
        $this->_id_category = filter_var($id_category, FILTER_SANITIZE_STRING);
    }

    /** 1:1 with FileEntity */
    public function setFile() {

        $this->File = \Ada\EntityRelation::hasOne($this, 'file', 'id_file');
    }
    /** 1:1 with UserEntity */
    public function setUser() {

        $this->User = \Ada\EntityRelation::hasOne($this, 'user', 'id_user');
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_file
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_file($id_file = 0): void {
        $this->_id_file = filter_var($id_file, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_user
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_user($id_user = 0): void {
        $this->_id_user = filter_var($id_user, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for share_date
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_share_date($share_date = ""): void {
        try {
            $this->_share_date = \helpers\Date::toDate($share_date);
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException("DateError", $ex);
        }
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_category
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_category() {
        return $this->_id_category;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_file
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_file() {
        return $this->_id_file;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_user
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_user() {
        return $this->_id_user;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for share_date
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_share_date() {
        return \helpers\Date::toString($this->_share_date, "d-m-Y H:i:s");
    }

}

