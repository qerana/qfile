<?php

/*
 * Copyright (C) 2020-21 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\entity;

use \Ada\EntityManager,
    Qerapp\qfile\model\file\interfaces\FileInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for File
  |*****************************************************************************
  |
  | Entity File
  | @author qDevTools,
  | @date 2020-06-19 21:43:20,
  |*****************************************************************************
 */

class FileEntity extends EntityManager implements FileInterface, \JsonSerializable
{

    public
    /** @object, Category entity */
            $Category,
            /** @object, User entity */
            $User,
            $Downloads = [],
            /** @float , file size in MB */
            $size,
            /** @string, mime type */
            $mime,
            $Share,
            $ShareUsers,
            $download,
            $fullpath,
            // tipo de mime para el content encode
            $data_img,
            // la iamgen en base 64
            $img_string;
    protected

//ATRIBUTES        

    /** @var int(11), $type  */
            $_type,
            /** @var text(), $description  */
            $_description,
            /** @var int(11), $id_file  */
            $_id_file,
            /** @var int(11), $id_category  */
            $_id_category,
            /** @var varchar(45), $id_user  */
            $_id_user,
            /** @var varchar(100), $title  */
            $_title,
            /** @var varchar(100), $file_name  */
            $_file_name,
            /** @var varchar(255), $path  */
            $_path,
            /** @var datetime(), $date_upload  */
            $_date_upload;

//RELATED

    public function __construct(array $data = [])
    {
        $this->populate($data);
    }

    /**
     *  1:1 to Category entity
     */
    public function setCategory()
    {
        $this->Category = \Ada\EntityRelation::hasOne($this, 'category', 'id_category');
    }

    /**
     * 1:n to Download entity 
     */
    public function setDownloads()
    {

        $this->Downloads = \Ada\EntityRelation::hasMany($this, 'download', 'id_file');
    }

    /**
     *  1:1 to Userentity
     */
    public function setUser()
    {
        $this->User = \Ada\EntityRelation::hasOne($this, 'user', 'id_user');
    }

    /**
     * 1:1 with share, only for control
     */
    public function setShare()
    {
        $this->Share = \Ada\EntityRelation::hasOne($this, 'share', 'id_file');
    }

    /**
     * 1:n with shares-users
     */
    public function setShareUsers()
    {
        $ShareUsers = \Ada\EntityRelation::hasMany($this, 'share', 'id_file');

        if (!empty($ShareUsers)) {

            foreach ($ShareUsers AS $ShareUser):
                $ShareUser->setUser();
            endforeach;
            $this->ShareUsers = $ShareUsers;
        }
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for type
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_type($type = 0): void
    {
        $this->_type = filter_var($type, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for description
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_description($description = ""): void
    {
        $this->_description = filter_var($description, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_file
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_file($id_file = 0): void
    {
        $this->_id_file = filter_var($id_file, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_category
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_category($id_category = 0): void
    {
        $this->_id_category = filter_var($id_category, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_user
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_id_user($id_user = ""): void
    {
        $this->_id_user = filter_var($id_user, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for title
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_title($title = ""): void
    {
        $this->_title = filter_var($title, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for file_name
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_file_name($file_name = ""): void
    {
        $this->_file_name = filter_var($file_name, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for path
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_path($path = ""): void
    {
        $this->_path = filter_var($path, FILTER_SANITIZE_STRING);
        $this->fullpath = realpath(__DATA__ . 'files/' . $this->_path . '/' . $this->_file_name);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for date_upload
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_date_upload($date_upload = ""): void
    {
        try {
            $this->_date_upload = \helpers\Date::toDate($date_upload);
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException("DateError", $ex);
        }
    }

    /**
     * Set file size
     * @return types
     */
    public function setFileSize()
    {

// size in KB
        $size_bytes = filesize(__DATA__ . '/files/' . $this->_path . '/' . $this->_file_name);

// size in MB
        $size_mbytes = ($size_bytes / 1024 / 1024);

        $this->size = number_format($size_mbytes, 2);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for type
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_type()
    {
        return $this->_type;
    }

    public function setMime()
    {
        $this->mime = mime_content_type(__DATA__ . '/files/' . $this->_path . '/' . $this->_file_name);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for description
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_description()
    {
        return filter_var($this->_description, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_file
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_file()
    {
        return $this->_id_file;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_category
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_category()
    {
        return $this->_id_category;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_user
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_id_user()
    {
        return filter_var($this->_id_user, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for title
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_title()
    {
        return filter_var($this->_title, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for file_name
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_file_name()
    {
        return filter_var($this->_file_name, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for path
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_path()
    {
        return filter_var($this->_path, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for date_upload
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_date_upload()
    {
           if (!is_null($this->_date_upload)) {
            return \helpers\Date::toString($this->_date_upload, "d-m-Y H:i:s");
        }
        //return $this->_date_upload;
    }

    /**
     *  Set file info
     */
    public function setFileInfo()
    {
        $this->setFileSize();
        $this->setMime();
    }

    /**
     *  Check if a file was download
     */
    public function checkDownload()
    {

        $this->setDownloads();
        if (empty($this->Downloads)) {
            $this->download = false;
        } else {
            $this->download = true;
        }
    }

    /**
     * Setea el encode type y convierte la imgen en string en base 64
     */
    public function setImgData()
    {
        // obtenemos el mime type
        $this->mime = mime_content_type($this->fullpath);

        // convertimos a un string en base 64
        $this->img_string = base64_encode(file_get_contents($this->fullpath));
    }

    public function getFullPath()
    {
        return realpath(__DATA__ . 'files/' . $this->_path . '/' . $this->_file_name);
    }

}
