<?php

/*
 * Copyright (C) 2020-21 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\entity;

use \Ada\EntityManager,
    Qerapp\qfile\model\file\interfaces\DownloadInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Download
  |*****************************************************************************
  |
  | Entity Download
  | @author qDevTools,
  | @date 2020-06-19 21:50:03,
  |*****************************************************************************
 */

class DownloadEntity extends EntityManager implements DownloadInterface, \JsonSerializable {

    public
            $File;
    protected

    //ATRIBUTES        

    /** @var int(11), $id_download  */
            $_id_download,
            /** @var int(11), $id_file  */
            $_id_file,
            /** @var varchar(45), $user  */
            $_user,
            /** @var varchar(45), $ip_address  */
            $_ip_address,
            /** @var datetime(), $date_download  */
            $_date_download,
            /** @var text(), $agent  */
            $_agent;

    //RELATED

    public function __construct(array $data = []) {
        $this->populate($data);

        //RELATED-LOAD
    }

//SETTERS

    /**
     *  1:1 with File
     */
    public function setFile() {
        $this->File = \Ada\EntityRelation::hasOne($this, 'file', 'id_file');
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_download
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_download($id_download = 0): void {
        $this->_id_download = filter_var($id_download, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_file
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_file($id_file = 0): void {
        $this->_id_file = filter_var($id_file, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for user
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_user($user = ""): void {
        $this->_user = filter_var($user, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for ip_address
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_ip_address($ip_address = ""): void {
        $this->_ip_address = filter_var($ip_address, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for date_download
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_date_download($date_download = ""): void {
        try {
            $this->_date_download = \helpers\Date::toDate($date_download);
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException("DateError", $ex);
        }
    }

    public function setIpAddress() {
        $this->_ip_address = filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP);
    }

    public function setAgent() {
        $this->_agent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS);
    }

    public function setDate() {
        $this->_date_download = date('Y-m-d H:i:s');
    }

    public function setUser() {
        $this->_user = $_SESSION['Q_username'];
    }

    public function setDefaults() {

        $this->setIpAddress();
        $this->setAgent();
        $this->setDate();
        $this->setUser();
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for agent
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_agent($agent = ""): void {
        $this->_agent = filter_var($agent, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_download
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_download() {
        return $this->_id_download;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_file
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_file() {
        return $this->_id_file;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for user
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_user() {
        return filter_var($this->_user, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for ip_address
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_ip_address() {
        return filter_var($this->_ip_address, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for date_download
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_date_download() {
        return \helpers\Date::toString($this->_date_download, "d-m-Y H:i:s");
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for agent
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_agent() {
        return filter_var($this->_agent, FILTER_SANITIZE_STRING);
    }

}
