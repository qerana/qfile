<?php

/*
 * Copyright (C) 2020-21 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file;

use Qerapp\qfile\model\file\entity\DownloadEntity,
    Qerapp\qfile\model\file\mapper\DownloadMapper,
    Qerapp\qfile\model\file\interfaces\DownloadMapperInterface,
    Qerapp\qfile\model\file\repository\DownloadRepository;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity DownloadService
  | @author qDevTools,
  | @date 2020-06-19 21:50:03,
  |*****************************************************************************
 */

class DownloadService {

    public
    //RELATED-MAPPER-OBJECT
            $DownloadRepository,
            /** @object entity Download */
            $Download;

    public function __construct(DownloadMapperInterface $Mapper = null) {

        //RELATED-MAPPER-OBJECT-NEW



        try {
            $DownloadMapper = new DownloadMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.DownloadService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $DownloadMapper : $Mapper;
        $this->DownloadRepository = new DownloadRepository($MapperRepository);
    }

    /**
     * Get last filesdownloads
     * @param int $limit
     */
    public function getLastDownloads(int $limit = 10) {

        $options =['limit' => $limit, 'orderby' => 'date_download DESC'];
        $Downloads = $this->DownloadRepository->findAllDownloads($options);
        foreach ($Downloads AS $Download):

            $Download->setFile();

        endforeach;

        return $Downloads;
    }

    /*
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */

    public function getAll($json = false) {

        $Collection = $this->DownloadRepository->findAll();
        if ($json) {
            echo json_encode($Collection);
        } else {
            return $Collection;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $Entity = $this->DownloadRepository->findById($id);
        if ($Entity) {
            if ($json) {
                echo json_encode($Entity);
            } else {
                return $Entity;
            }
        } else {
            \QException\Exceptions::showHttpStatus(404, 'Download ' . $id . ' NOT FOUND!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save  array data
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;

        $this->Download = new DownloadEntity($data_to_save);
        $this->saveDownload($this->Download);
    }

    /**
     * -------------------------------------------------------------------------
     * Save  Object
     * -------------------------------------------------------------------------
     */
    public function saveDownload($Download) {

        $this->DownloadRepository->store($Download);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id) {
        return $this->DownloadRepository->remove($id);
    }

}
