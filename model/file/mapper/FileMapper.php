<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qfile\model\file\entity\FileEntity,
    Qerapp\qfile\model\file\interfaces\FileMapperInterface,
    Qerapp\qfile\model\file\interfaces\FileInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for FileMapperEntity
  |*****************************************************************************
  |
  | MAPPER FileMapper
  | @author qDevTools,
  | @date 2020-06-19 21:43:20,
  |*****************************************************************************
 */

class FileMapper extends AdaDataMapper implements FileMapperInterface {

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null) {

        $this->_Adapter = (is_null($Adapter) ) ? new PDOAdapter(\Ada\SqlPDO::singleton(), 'qpfiles') : $Adapter;

        //RELATED-MAPPER-OBJECT


        parent::__construct($this->_Adapter);
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id) {
        $row = $this->_Adapter->find(['id_file' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(FileInterface $File) {

        $data = parent::getDataObject($File);

        if (is_null($File->id_file)) {
            $File->id_file = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_file' => $File->id_file]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($File) {

        // if $id is a object and a UserEntity  
        if ($File instanceof FileInterface) {
            $File = $File->id_file;
        }

        return $this->_Adapter->delete(['id_file' => $File]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): FileEntity {
        $FileEntity = new FileEntity($row);
        return $FileEntity;
    }

}
