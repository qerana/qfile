<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\mapper;

use 
    Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qfile\model\file\entity\DownloadEntity,
    Qerapp\qfile\model\file\interfaces\DownloadMapperInterface,
    Qerapp\qfile\model\file\interfaces\DownloadInterface;
    
//RELATED-NAMESPACES    
    
/*
  |*****************************************************************************
  | MAPPER CLASS for DownloadMapperEntity
  |*****************************************************************************
  |
  | MAPPER DownloadMapper
  | @author qDevTools,
  | @date 2020-06-19 21:50:03,
  |*****************************************************************************
 */

class DownloadMapper extends AdaDataMapper implements DownloadMapperInterface  {

    //RELATED-MAPPERS
    
     public function __construct(AdapterInterface $Adapter = null)
    {

          $this->_Adapter = (is_null($Adapter) ) ? new PDOAdapter(\Ada\SqlPDO::singleton(),'qfile_downloads') : $Adapter;
         
         //RELATED-MAPPER-OBJECT
         
         
        parent::__construct($this->_Adapter);
        
    }
    
    
    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */
    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_download' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }
    
    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(DownloadInterface $Download){
        
        $data = parent::getDataObject($Download);
        
        if (is_null($Download->id_download)) { 
$Download->id_download = $this->_Adapter->insert($data);
} else {
$this->_Adapter->update($data, ['id_download' => $Download->id_download]);
}
        
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($Download)
    {

        // if $id is a object and a UserEntity  
        if ($Download instanceof DownloadInterface) {
            $Download = $Download->id_download;
        }

        return $this->_Adapter->delete(['id_download' => $Download]);
    }
    
        /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): DownloadEntity
    {
        $DownloadEntity = new DownloadEntity($row);
        return $DownloadEntity;
    }
 
 
}