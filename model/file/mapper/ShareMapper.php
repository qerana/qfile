<?php

/*
 * Copyright (C) 2020-2021 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qfile\model\file\entity\ShareEntity,
    Qerapp\qfile\model\file\interfaces\ShareMapperInterface,
    Qerapp\qfile\model\file\interfaces\ShareInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for ShareMapperEntity
  |*****************************************************************************
  |
  | MAPPER ShareMapper
  | @author qDevTools,
  | @date 2020-10-27 04:22:50,
  |*****************************************************************************
 */

class ShareMapper extends AdaDataMapper implements ShareMapperInterface {

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null) {

        $this->_Adapter = (is_null($Adapter) ) ? new PDOAdapter(\Ada\SqlPDO::singleton(), 'qfile_shares') : $Adapter;

        //RELATED-MAPPER-OBJECT


        parent::__construct($this->_Adapter);
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id) {
        $row = $this->_Adapter->find(['id_file' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(ShareInterface $Share) {

        $data = parent::getDataObject($Share);

        $this->_Adapter->insert($data);
    }

    /**
     * -------------------------------------------------------------------------
     * Update  entity
     * -------------------------------------------------------------------------
     */
    public function update(ShareInterface $Share, $condition) {

        $data = parent::getDataObject($Share);

        $this->_Adapter->update($data, $condition);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function deleteShare(int $id_file, int $id_user) {

        return $this->_Adapter->delete(['id_file' => $id_file, 'id_user' => $id_user]);
    }

    /**
     * Delete file shared
     * @param int $id_file
     * @return type
     */
    public function deleteFileShared(int $id_file) {
        return $this->_Adapter->delete(['id_file' => $id_file]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): ShareEntity {
        $ShareEntity = new ShareEntity($row);
        return $ShareEntity;
    }

}
