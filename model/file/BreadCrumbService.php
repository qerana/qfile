<?php

/*
 * Copyright (C) 2010 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file;

use Qerapp\qfile\model\file\interfaces\CategoryInterface,
    Qerapp\qfile\model\file\repository\CategoryRepository,
    Qerapp\qfile\model\file\mapper\CategoryMapper;

/**
 * Description of BreadCrumService
 *
 * @author diemarc
 */
class BreadCrumbService {

    protected

    /**  @CategoryEntity  , the folder to extract the breadcrumbs */
            $Folder,
            /** @array, collection of breadcrumbs */
            $breads = [];

    public function __construct(CategoryInterface $Category) {
        $this->Folder = $Category;

        // parse breadcrumbs
        $this->parseBreadcrumbFolder($this->Folder->id_category);
    }

    public function parseBreadcrumbFolder(int $id_category = 0) {


        $CategoryRepo = new CategoryRepository(new CategoryMapper);
        $Categories = $CategoryRepo->findById_category($id_category);

        foreach ($Categories AS $Category):

            $Category->Parents = $this->parseBreadcrumbFolder($Category->parent_category);
            $this->breads[] = $Category;

        endforeach;
    }

    /**
     * Get breads
     * @return type
     */
    public function getBreads() {
        return $this->breads;
    }

}

