<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file;

use Qerapp\qfile\model\file\interfaces\FileInterface,
    Qerapp\qfile\model\file\DownloadService,
    Qerapp\qfile\model\file\entity\DownloadEntity;

/*
  |*****************************************************************************
  | FileHandler
  |*****************************************************************************
  | Upload,create paths, move, delete files on disks
  | @author qDevTools,
  | @date 2020-06-19 21:43:20,
  |*****************************************************************************
 */

class FileHandler
{

    public
             $is_image;
    
    private
    /**
     *  @Entity File Entity 
     */
            $File,
            $img_mimes = [
                'image/gif',
                'image/jpeg',
                'image/png'
    ];
    protected
            $_file,
            $_tmp_file,
            $_file_extension,
            $_base_path,
            $_path_file,
            $_fullpath,
            $_sw_rename_file = true,
            $_sw_register_download = true;

    public function __construct(FileInterface $File)
    {
        $this->File = $File;
        $this->File->setCategory();
        $this->_base_path = realpath(__DATA__ . '/files/');
    }

    /**
     *  Base path, __DATA__/files , default
     * @param string $_base_path
     * @return void
     */
    public function set_base_path(string $_base_path): void
    {
        $this->_base_path = $_base_path;
    }

    /**
     *  Path file, next of  base_path;
     * @param string $_path_file
     * @return void
     */
    public function set_path_file(string $_path_file): void
    {

        $file_fullpath = $this->_base_path . '/' . $_path_file . '/' . $this->File->Category->category;


        if (!is_dir($file_fullpath)) {
            mkdir($file_fullpath, 0777, true);
        }
        $this->_fullpath = $file_fullpath;
        $this->_path_file = $_path_file;
        $this->File->set_path($_path_file . $this->File->Category->category);
    }

    /**
     * rename or not filename
     * @param bool $_sw_rename_file
     * @return void
     */
    public function set_sw_rename_file(bool $_sw_rename_file): void
    {
        $this->_sw_rename_file = $_sw_rename_file;
    }

    /**
     * Register download or not
     * @param bool $_sw_register_download
     * @return void
     */
    public function set_sw_register_download(bool $_sw_register_download): void
    {
        $this->_sw_register_download = $_sw_register_download;
    }

    /**
     * Set file name, if sw_rename_file is true, then generate alphanumeric random name, else only
     * clean the file name
     * @param string $_file
     * @return void
     */
    public function set_file(string $_file): void
    {

        if ($this->_sw_rename_file) {
            $this->_file = $this->File->Category->category . '_' . \helpers\Security::random(4);
        } else {
            $this->_file = \helpers\Utils::cleanString($_file);
        }
    }

    /**
     *  Process file form FROM multipat
     */
    public function uploadFormFile()
    {

        $file_info = pathinfo($_FILES['file']['name']);
        $this->_file_extension = $file_info['extension'];

        $this->set_file($file_info['filename']);

        // mutate file entity
        // set the file entity name
        $this->File->set_file_name($this->_file . '.' . $this->_file_extension);

        // if file title is not setted then set the filename form
        if ($this->File->title == '') {
            $this->File->set_title($file_info['filename']);
        }

        $this->_tmp_file = $_FILES['file']['tmp_name'];

        // si es imagen comprueba tipos de archivos permitidos
        if ($this->is_image) {
            $mime_content = mime_content_type($this->_tmp_file);

            // comprobamos tipo de archivo
            if (!in_array($mime_content, $this->img_mimes)) {
                throw new \RuntimeException('Tipo de archivo no permitido, solo (png,gif,jpg)');
            }
        }



        // upload
        $this->uploadFile();
    }

    /**
     *  Upload file
     */
    public function uploadFile()
    {


        $fullpath_with_name = $this->_fullpath . '/' . $this->_file . '.' . $this->_file_extension;

        if (!move_uploaded_file($this->_tmp_file, $fullpath_with_name)) {
            \QException\Exceptions::showError('Qfile.UploadFile', 'Fail to upload files to ' . $fullpath_with_name);
        }
    }

    /**
     *  Get file real paths
     * @return type
     */
    public function getRealPath()
    {
        return realpath($this->_base_path . '/' . $this->File->path . '/' . $this->File->get_file_name());
    }

    /**
     *  Delete a file
     */
    public function delete()
    {
        $file = $this->getRealPath();

        if (!empty($file)) {
            unlink($file);
        }
    }

    /**
     *  Download  a file
     */
    public function downloadFile()
    {

        $file = $this->getRealPath();


        // get the name and fiel extension
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        // clean document name, to fix browser incompatibility
        $show_name = preg_replace("/[^a-zA-Z]+/", "", $this->File->title) . '.' . $ext;

        // if file not exists...
        if (empty($file)) {
            \QException\Exceptions::showError('Error de descarga', 'No se puede descargar el archivo, ruta no exista!!');
        } else {
            $type = mime_content_type($file);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: inline; filename=' . basename($show_name));
            header("Content-Type:$type"); // Send type of file
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            // register download
            $this->registerDownload();
            exit;
        }
    }
    

    /**
     *  Register downloads
     */
    public function registerDownload()
    {

        $Download = new DownloadEntity;
        $Download->id_file = $this->File->id_file;
        $Download->setDefaults();

        $DownloadService = new DownloadService;
        $DownloadService->saveDownload($Download);
    }

}
