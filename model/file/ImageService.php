<?php

/*
 * Copyright (C) 2021  diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file;

use Gumlet\ImageResize;

/*
  |*****************************************************************************
  | ImagesService, handler for image tasks, resize, etc
  |*****************************************************************************
  | @require  "gumlet/php-image-resize": "1.9.*"
  | @url https://github.com/gumlet/php-image-resize
  | @author diemarc,
  | @date 2021-02-04
  |*****************************************************************************
 */

class ImageService
{

    public
            $scale,
            $height,
            $width,
            $output,
            // true crea otro fichero de imagen modificado
            $replace = false;
    private

    /** ImageResize Object */
            $Image,
            // string full path donde esta la foto a manipular
            $image_path,
            // mimes soportados
            $mimes = [
                'image/gif',
                'image/jpeg',
                'image/png'
    ];
    protected
            $FileService;

    public function __construct(string $image)
    {

        $this->image_path = $image;
        $mime_content = mime_content_type($this->image_path);

        // comprobamos tipo de archivo
        if (!in_array($mime_content, $this->mimes)) {
            throw new \RuntimeException('Tipo de archivo no permitido, solo (png,gif,jpg)');
        }

        $this->Image = new ImageResize($this->image_path);
    }

    /**
     * Realiza la operacion seleccionada
     * @param string $mode
     */
    public function go(string $mode)
    {

        switch ($mode) {
            case 'BestFit';
                return $this->resizeBest();
                break;
            case 'ForceFit';
                return $this->resize();
                break;
            case 'Scale';
                return $this->scale();
                break;
        }
    }

    /**
     * Save image
     */
    public function resizeBest()
    {

        try {
            $this->Image->resizeToBestFit($this->width, $this->height);
            return $this;
        } catch (\ImageResizeException $ex) {
            \QException\Exceptions::ShowException('Error al dimensionar la imagen', $ex);
        }
    }

    public function resize()
    {

        try {
            $this->Image->resize($this->width, $this->height);
            return $this;
        } catch (\ImageResizeException $ex) {
            \QException\Exceptions::ShowException('Error al dimensionar la imagen', $ex);
        }
    }

    /**
     * Escala una iamgen
     * @return $this
     */
    public function scale()
    {

        try {
            $this->Image->scale($this->scale);
            return $this;
        } catch (\ImageResizeException $ex) {
            \QException\Exceptions::ShowException('Error al escalar imagen', $ex);
        }
    }

    /*
     * 
     * Devuelve por pantalla
     */

    public function screen()
    {
        $this->Image->output();
    }

    /**
     * Reemplaza el fichero
     * @param string $prefix , es el prefijo que se le quiere agregar a la imagen, para guardarlo
     * con otro nombre , asi mantener la imagen original y la redimensionada
     */
    public function save(string $prefix = '')
    {

        // si se ha puesto el prefijo
        if (!empty($prefix)) {

            // obtenemos el path ingo
            $path_info = pathinfo($this->image_path);

            // trozeamos y agregamos el prefijo antes del nombre del fichero
            $path_to_save = $path_info['dirname'] . '/' . $prefix . '_' . $path_info['filename'] . '.' . $path_info['extension'];
        }
        // si es vacio se reemplaza el archivo
        else {
            $path_to_save = $this->image_path;
        }

        $this->Image->save($path_to_save);
    }

}
