<?php

/*
 * Copyright (C) 2020-21 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\repository;

use Qerapp\qfile\model\file\interfaces\DownloadMapperInterface,
    Qerapp\qfile\model\file\interfaces\DownloadRepositoryInterface,
    Qerapp\qfile\model\file\interfaces\DownloadInterface;

/*
  |*****************************************************************************
  | DownloadRepositoryRepository
  |*****************************************************************************
  |
  | Repository DownloadRepository
  | @author qDevTools,
  | @date 2020-06-19 21:50:03,
  |*****************************************************************************
 */

class DownloadRepository implements DownloadRepositoryInterface {

    private
            $_DownloadMapper;

    public function __construct(DownloadMapperInterface $Mapper) {

        $this->_DownloadMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all DownloadRepository
     * -------------------------------------------------------------------------
     * @return DownloadRepositoryEntity collection
     */
    public function findById(int $id) {
        return $this->_DownloadMapper->findOne(['id_download' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all DownloadRepository
     * -------------------------------------------------------------------------
     * @return DownloadRepositoryEntity collection
     */
    public function findAll(array $conditions = [], array $options = []) {
        return $this->_DownloadMapper->findAll($conditions, ['orderby' => 'date_download DESC']);
    }

    /**
     * find all downloads
     * @param array $conditions
     * @param array $options
     * @return types
     */
    public function findAllDownloads(array $options = []) {
        return $this->_DownloadMapper->findAll([], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_download
     * ------------------------------------------------------------------------- 
     * @param id_download 
     */
    public function findById_download(int $id_download, array $options = []) {
        return $this->_DownloadMapper->findAll(['id_download' => $id_download], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_file
     * ------------------------------------------------------------------------- 
     * @param id_file 
     */
    public function findById_file(int $id_file, array $options = []) {
        return $this->_DownloadMapper->findAll(['id_file' => $id_file], ['orderby' => 'date_download DESC']);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  user
     * ------------------------------------------------------------------------- 
     * @param user 
     */
    public function findByUser(string $user, array $options = []) {
        return $this->_DownloadMapper->findAll(['user' => $user], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  ip_address
     * ------------------------------------------------------------------------- 
     * @param ip_address 
     */
    public function findByIp_address(string $ip_address, array $options = []) {
        return $this->_DownloadMapper->findAll(['ip_address' => $ip_address], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  date_download
     * ------------------------------------------------------------------------- 
     * @param date_download 
     */
    public function findByDate_download(string $date_download, array $options = []) {
        return $this->_DownloadMapper->findAll(['date_download' => $date_download], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  agent
     * ------------------------------------------------------------------------- 
     * @param agent 
     */
    public function findByAgent(string $agent, array $options = []) {
        return $this->_DownloadMapper->findAll(['agent' => $agent], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save DownloadRepository
     * -------------------------------------------------------------------------
     * @object $DownloadRepositoryEntity
     * @return type
     */
    public function store(DownloadInterface $DownloadRepositoryEntity) {
        return $this->_DownloadMapper->save($DownloadRepositoryEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete DownloadRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id) {
        return $this->_DownloadMapper->delete($id);
    }

}
