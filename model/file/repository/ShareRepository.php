<?php

/*
 * Copyright (C) 2020-21 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\repository;

use Qerapp\qfile\model\file\interfaces\ShareMapperInterface,
    Qerapp\qfile\model\file\interfaces\ShareRepositoryInterface,
    Qerapp\qfile\model\file\interfaces\ShareInterface;

/*
  |*****************************************************************************
  | ShareRepositoryRepository
  |*****************************************************************************
  |
  | Repository ShareRepository
  | @author qDevTools,
  | @date 2020-10-27 04:22:50,
  |*****************************************************************************
 */

class ShareRepository implements ShareRepositoryInterface {

    private
            $_ShareMapper;

    public function __construct(ShareMapperInterface $Mapper) {

        $this->_ShareMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get One
     * -------------------------------------------------------------------------
     * @return ShareRepository
     */
    public function find(array $condition = [], array $options = []) {
        return $this->_ShareMapper->findOne($condition, $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all ShareRepository
     * -------------------------------------------------------------------------
     * @return ShareRepositoryEntity collection
     */
    public function findById(int $id) {
        return $this->_ShareMapper->findOne(['' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all ShareRepository
     * -------------------------------------------------------------------------
     * @return ShareRepositoryEntity collection
     */
    public function findAll(array $conditions = [], array $options = []) {
        return $this->_ShareMapper->findAll($conditions, $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_file
     * ------------------------------------------------------------------------- 
     * @param id_file 
     */
    public function findById_file(int $id_file, array $options = []) {
        return $this->_ShareMapper->findAll(['id_file' => $id_file], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_user
     * ------------------------------------------------------------------------- 
     * @param id_user 
     */
    public function findById_user(int $id_user, array $options = []) {
        return $this->_ShareMapper->findAll(['id_user' => $id_user], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_user
     * ------------------------------------------------------------------------- 
     * @param id_user 
     */
    public function findById_category(int $id_category, array $options = []) {
        return $this->_ShareMapper->findAll(['id_category' => $id_category], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  share_date
     * ------------------------------------------------------------------------- 
     * @param share_date 
     */
    public function findByShare_date(string $share_date, array $options = []) {
        return $this->_ShareMapper->findAll(['share_date' => $share_date], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save ShareRepository
     * -------------------------------------------------------------------------
     * @object $ShareRepositoryEntity
     * @return type
     */
    public function store(ShareInterface $ShareRepositoryEntity) {
        return $this->_ShareMapper->save($ShareRepositoryEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Updates ShareRepository
     * -------------------------------------------------------------------------
     * @object $ShareRepositoryEntity
     * @return type
     */
    public function modify(ShareInterface $ShareRepositoryEntity, $condition) {
        return $this->_ShareMapper->update($ShareRepositoryEntity, $condition);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete ShareRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove(int $id_file, int $id_user) {
        return $this->_ShareMapper->delete($id_file, $id_user);
    }

    /**
     * Delete file shared, remove all user association
     * @param int $id_file
     * @return type
     */
    public function unshareFile(int $id_file) {
        return $this->_ShareMapper->deleteFileShared($id_file);
    }

}
