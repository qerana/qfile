<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\repository;
use Qerapp\qfile\model\file\interfaces\FileMapperInterface,
Qerapp\qfile\model\file\interfaces\FileRepositoryInterface,
Qerapp\qfile\model\file\interfaces\FileInterface;
/*
  |*****************************************************************************
  | FileRepositoryRepository
  |*****************************************************************************
  |
  | Repository FileRepository
  | @author qDevTools,
  | @date 2020-06-19 21:43:20,
  |*****************************************************************************
 */

class FileRepository implements FileRepositoryInterface
{
    
    private
            $_FileMapper;
            
    public function __construct(FileMapperInterface $Mapper)
    {
        
         $this->_FileMapper = $Mapper;
        
    }
    
    
      /**
     * -------------------------------------------------------------------------
     * Get all FileRepository
     * -------------------------------------------------------------------------
     * @return FileRepositoryEntity collection
     */
    
    public function findById(int $id)
    {
        return $this->_FileMapper->findOne(['id_file'=>$id]);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get all FileRepository
     * -------------------------------------------------------------------------
     * @return FileRepositoryEntity collection
     */
    
    public function findAll(array $conditions = [],array $options = [])
    {
        return $this->_FileMapper->findAll($conditions,$options);
    }
    
    
    /** 
* ------------------------------------------------------------------------- 
* Fin by  id_file
* ------------------------------------------------------------------------- 
* @param id_file 
*/ 
  public function findById_file(int  $id_file,array $options = [])
{ 
return $this->_FileMapper->findAll(['id_file'=> $id_file],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  id_category
* ------------------------------------------------------------------------- 
* @param id_category 
*/ 
  public function findById_category(int  $id_category,array $options = [])
{ 
return $this->_FileMapper->findAll(['id_category'=> $id_category],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  id_user
* ------------------------------------------------------------------------- 
* @param id_user 
*/ 
  public function findById_user(string $id_user,array $options = [])
{ 
return $this->_FileMapper->findAll(['id_user'=> $id_user],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  title
* ------------------------------------------------------------------------- 
* @param title 
*/ 
  public function findByTitle(string $title,array $options = [])
{ 
return $this->_FileMapper->findAll(['title'=> $title],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  file_name
* ------------------------------------------------------------------------- 
* @param file_name 
*/ 
  public function findByFile_name(string $file_name,array $options = [])
{ 
return $this->_FileMapper->findAll(['file_name'=> $file_name],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  path
* ------------------------------------------------------------------------- 
* @param path 
*/ 
  public function findByPath(string $path,array $options = [])
{ 
return $this->_FileMapper->findAll(['path'=> $path],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  date_upload
* ------------------------------------------------------------------------- 
* @param date_upload 
*/ 
  public function findByDate_upload(string $date_upload,array $options = [])
{ 
return $this->_FileMapper->findAll(['date_upload'=> $date_upload],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  user_upload
* ------------------------------------------------------------------------- 
* @param user_upload 
*/ 
  public function findByUser_upload(string $user_upload,array $options = [])
{ 
return $this->_FileMapper->findAll(['user_upload'=> $user_upload],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  desc
* ------------------------------------------------------------------------- 
* @param desc 
*/ 
  public function findByDesc(string $desc,array $options = [])
{ 
return $this->_FileMapper->findAll(['desc'=> $desc],$options);
}
    
   
    
     /**
     * -------------------------------------------------------------------------
     * Save FileRepository
     * -------------------------------------------------------------------------
     * @object $FileRepositoryEntity
     * @return type
     */
    public function store(FileInterface $FileRepositoryEntity)
    {
        return $this->_FileMapper->save($FileRepositoryEntity);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete FileRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_FileMapper->delete($id);
    }
 
}