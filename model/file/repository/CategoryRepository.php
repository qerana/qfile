<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qfile\model\file\repository;

use Qerapp\qfile\model\file\interfaces\CategoryMapperInterface,
    Qerapp\qfile\model\file\interfaces\CategoryRepositoryInterface,
    Qerapp\qfile\model\file\interfaces\CategoryInterface;

/*
  |*****************************************************************************
  | CategoryRepositoryRepository
  |*****************************************************************************
  |
  | Repository CategoryRepository
  | @author qDevTools,
  | @date 2020-06-19 21:42:00,
  |*****************************************************************************
 */

class CategoryRepository implements CategoryRepositoryInterface {

    private
            $_CategoryMapper;

    public function __construct(CategoryMapperInterface $Mapper) {

        $this->_CategoryMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all CategoryRepository
     * -------------------------------------------------------------------------
     * @return CategoryRepositoryEntity collection
     */
    public function findById(int $id) {
        return $this->_CategoryMapper->findOne(['id_category' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all CategoryRepository
     * -------------------------------------------------------------------------
     * @return CategoryRepositoryEntity collection
     */
    public function findAll(array $conditions = [], array $options = []) {
        
        return $this->_CategoryMapper->findAll($conditions, $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_category
     * ------------------------------------------------------------------------- 
     * @param id_category 
     */
    public function findById_category(int $id_category, array $options = []) {
        return $this->_CategoryMapper->findAll(['id_category' => $id_category], $options);
    }
    /**
     * ------------------------------------------------------------------------- 
     * Fin by  parent_category
     * ------------------------------------------------------------------------- 
     * @param id_category 
     */
    public function findBy_parent_category($parent_category, array $options = []) {
        
        $id_category = $parent_category['id_category'];
        
        return $this->_CategoryMapper->findAll(['parent_category' => $id_category], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  category
     * ------------------------------------------------------------------------- 
     * @param category 
     */

    public function findByCategory(string $category, array $options = []) {
        return $this->_CategoryMapper->findAll(['category' => $category], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  attr
     * ------------------------------------------------------------------------- 
     * @param attr 
     */

    public function findByAttr(string $attr, array $options = []) {
        return $this->_CategoryMapper->findAll(['attr' => $attr], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save CategoryRepository
     * -------------------------------------------------------------------------
     * @object $CategoryRepositoryEntity
     * @return type
     */
    public function store(CategoryInterface $CategoryRepositoryEntity) {
        return $this->_CategoryMapper->save($CategoryRepositoryEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete CategoryRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id) {
        return $this->_CategoryMapper->delete($id);
    }

}
