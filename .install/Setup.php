<?php

/*
 * This file is part of QAccess
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Setup {

    public function run() {

        // create  /_data_/file folder

        $base_dir = realpath(__DATA__ . '/files');
        if ($base_dir == '') {
            mkdir(__DATA__ . '/files', 0777, true);
        }


        $dir_target = realpath(__DOCUMENTROOT__) . '/src/js/app/qfile/';
        // create target folder
        mkdir($dir_target, 0777, true);

        $dir_source = realpath(__QERAPPSFOLDER__ . 'qfile/.install/_data/');
        shell_exec('cp -r ' . $dir_source . '/category.js ' . $dir_target);
        shell_exec('cp -r ' . $dir_source . '/file.js ' . $dir_target);

        $output = '============================================================' . "\n";
        $output .= " Qfile successfull installed \n";
        $output .= '-----------------------------------------------------------' . "\n";
        $output .= " This qerapp is not visible on nav, please edit xml module and set visible to 1 \n";
        $output .= " If ACL is actived run #./cliqer acl:add --user=your_user--url=/qfile/category/  and /qfile/file/ \n";
        $output .= " To enable to user to download a file, add this url to ACL /qfile/file/download/ \n";
        $output .= '============================================================' . "\n";

        echo $output;
    }

}
