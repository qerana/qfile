//function uploadFile(form) {
//
//
//    form.ajaxForm({
//
//        success: function () {
//            $('#modalLg').modal('hide');
//            loadFile();
//        }
//    });
//
//
//}


// load all data
function loadFile() {
    getDataJson('/qfile/File/getAllInJson', 'fill()');
}

// fill index table
function fill() {

    var list_File = '';
    var total_mb = 0;
    $.each(data_response, function (i, File) {
        
        // sum the file size
        total_mb += File.size * 1;
        
        list_File += '<tr class="small">';
        list_File += '<td>\n\
                            <a href="/qfile/File/detail/' + File.id_file + '" class="btn btn-outline-primary btn-sm" title="Detalle File"><i class="fa fa-tasks"></i></a>\n\
                            <a href="/qfile/File/download/' + File.id_file + '" class="btn btn-outline-info btn-sm" title="Download File" target="_blank"><i class="fa fa-download"></i></a>';

        list_File += '<td>' + File.Category.category + '</td>';
        list_File += '<td>' + File.title + '</td>';

        list_File += '</tr>';

    });
    $('#size').html(total_mb + ' MB');
    $('#list_File').html(list_File);

}

// load data
function loadDataFile() {

    var id_file = $('#data_File').attr("data-File");
    getDataJson('/qfile/File/getOneInJson/' + id_file, 'fillDataFile()');

}


// fill data detail
function fillDataFile() {

    var data_json = ' ';
    var detail_title = '';

    $.each(data_response, function (i, File) {


        data_json += ' <div class="row m-1">';
        data_json += '    <div class="col-sm-3 bg-gray-100 p-2">';
        data_json += '        <b>' + i + '</b>';
        data_json += '    </div>';
        data_json += '    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_' + i + '">';
        data_json += '        ' + File + '';
        data_json += '    </div>';
        data_json += '</div>';

    });


    $('#detail_title').html(detail_title);
    $('#data_File').html(data_json);

}