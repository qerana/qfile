/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  diemarc
 * Created: Jun 19, 2020
 */

CREATE TABLE `qpfiles` (
  `id_file` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `id_user` varchar(45) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_upload` datetime NOT NULL,
  `description` text,
  PRIMARY KEY (`id_file`,`id_category`),
  KEY `idx_id_category` (`id_category`)
) ENGINE=MyISAM AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='File store';


CREATE TABLE `qfile_categories` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `parent_category` int(11) NOT NULL DEFAULT '0',
  `category` varchar(200) NOT NULL,
  `attr` text,
  PRIMARY KEY (`id_category`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;



CREATE TABLE `qfile_downloads` (
  `id_download` int(11) NOT NULL AUTO_INCREMENT,
  `id_file` int(11) NOT NULL,
  `user` varchar(45) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `date_download` datetime DEFAULT NULL,
  `agent` text,
  PRIMARY KEY (`id_download`,`id_file`),
  KEY `idx_id_file` (`id_file`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

