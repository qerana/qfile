function uploadFile(form) {


    form.ajaxForm({

        success: function () {
            $('#modalLg').modal('hide');
            $('#file').val('');
            loadFiles($('#f_id_category').val());
        }
    });


}
// load all data
function loadCategory() {
    getDataJson('/qfile/Category/getAllInJson', 'fill()');
}



// fill index table
function fill() {

    var list_Category = '';

    $.each(data_response, function (i, Category) {
        list_Category += '<tr class="small">';
        list_Category += '<td><a href="/qfile/Category/detail/' + Category.id_category + '" class="btn btn-outline-primary btn-sm" title="Detalle Category"><i class="fa fa-tasks"></i></a>';

        list_Category += '<td>' + Category.id_category + '</td>';
        list_Category += '<td>' + Category.category + '</td>';
        list_Category += '<td>' + Category.attr + '</td>';

        list_Category += '</tr>';

    });
    $('#list_Category').html(list_Category);

}

/**
 * Load  files
 * @returns {undefined}
 */
function loadFiles(id) {
    getDataJson('/qfile/file/getAllInJson/' + id, 'fillFilesList()');


}


// fill files category
function fillFilesList() {


    var data_json = ' ';
    var detail_title = '';

    $.each(data_response, function (i, File) {


        data_json += '<tr class="small">';
        data_json += '  <td>';
        data_json += '                      <a href="#" ';
        data_json += ' data-remote="/qfile/File/property/' + File.id_file + '"';
        data_json += ' data-target="#modalSec" ';
        data_json += ' data-toggle="modal" ';
        data_json += ' data-titlemodal="Detalle archivo" ';
        data_json += ' class="btn btn-outline-dark  btn-sm';
        data_json += '  title="File properties">';
        data_json += '                          <i class="fas fa-tasks"></i>';
        data_json += '                      </a> ';
        data_json += '  </td> ';
        data_json += '  <td><a href="/qfile/File/download/' + File.id_file + '" target="_blank" >' + File.title + '</a></td>';
        data_json += '  <td><small>' + File.mime + '</small></td>';
        data_json += '  <td><small>' + File.size + '</small> MB</td>';
        data_json += '</tr>';

    });


    $('#data_files_category').html(data_json);

}



function loadFilesCategory() {
    var id_category = $('#data_category').attr("data-id_category");
    getDataJson('/qfile/file/getAllInJson/' + id_category, 'fillFilesCategory()');


}

// fill files category
function fillFilesCategory() {

    var data_json = ' ';
    var detail_title = '';

    $.each(data_response, function (i, File) {


        data_json += '<div class="col-xl-3 col-md-6 mb-4">';
        data_json += '     <div class="card border-left-default shadow h-100 py-2">';
        data_json += '          <div class="card-body">'; // strart card body
        data_json += '               <div class="row no-gutters align-items-center">';
        data_json += '                  <div class="col mr-2">';
        data_json += '                       <div class="text-xs font-weight-bold text-info text-uppercase mb-1">';
        data_json += File.title;
        data_json += '                          <br><small class="font-weight-light text-gray-800">' + File.mime + ' </small>';
        data_json += '                      </div>';
        data_json += '                  </div>';
        data_json += '              </div>';
        data_json += '          </div>'; // end card body
        data_json += '           <div class="card-footer">';

        data_json += '                      <a href="/qfile/File/detail/' + File.id_file + '" class="btn btn-outline-dark  btn-sm';
        data_json += '                      target="_blank" title="File properties">';
        data_json += '                          <i class="fas fa-tasks"></i>';
        data_json += '                      </a> ';
        data_json += '                      <a href="/qfile/File/download/' + File.id_file + '" class="btn btn-outline-primary btn-sm"';
        data_json += '                      target="_blank" title="Download File">';
        data_json += '                          <i class="fas fa-download"></i>';
        data_json += '                      </a> <small class="font-weight-light text-gray-700"><b>' + File.size + 'MB </b></small>';
        data_json += '          </div>';
        data_json += '    </div>';
        data_json += '</div>';

    });


    $('#data_files_category').html(data_json);

}




// load data
function loadDataCategory() {

    var id_category = $('#data_Category').attr("data-Category");
    getDataJson('/qfile/Category/getOneInJson/' + id_category, 'fillDataCategory()');

}
// fill data detail
function fillDataCategory() {

    var data_json = ' ';
    var detail_title = '';

    $.each(data_response, function (i, Category) {


        data_json += '  <div class="col-xl-3 col-md-6 mb-4">';
        data_json += '    <div class="col-sm-3 bg-gray-100 p-2">';
        data_json += '        <b>' + i + '</b>';
        data_json += '    </div>';
        data_json += '    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_' + i + '">';
        data_json += '        ' + Category + '';
        data_json += '    </div>';
        data_json += '</div>';

    });


    $('#detail_title').html(detail_title);
    $('#data_Category').html(data_json);

}