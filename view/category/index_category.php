<div class="container-fluid">

    <section class="content-header">
        <h5 class="text-dark">
            <i class="fa fa-folder fa-2x"></i> Root folder
        </h5>
        <ol class="breadcrumb">
            <li>
                <input type="text" class="form-control input-sm" 
                       id="filter_search" 
                       name="filter_search"
                       aria-describedby="inputSuccess3Status"
                       value="" placeholder="Search"/>
            </li>
        </ol>

    </section> 


    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

            <a href="#" class="btn btn-secondary btn-sm"
               data-target="#modalLg" 
               data-remote="/qfile/Category/add"
               data-toggle="modal"
               data-titlemodal='new folder'
               >
                <span class="icon">
                    <i class="fas fa-plus-square"></i>
                </span>
            </a>
            <div class="dropdown no-arrow">
              <?php echo count($Categories); ?> folders
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <div class="row" id="" >
                <?php foreach ($Categories AS $Category): ?>
                    <div class="search_category col-xl-2 col-md-2 mb-3">
                        <div class="card border-left-default shadow">
                            <div class="card-body ">
                                <a href="/qfile/category/detail/<?php echo $Category->id_category; ?>"
                                   class="text-primary">
                                    <i class="fas fa-folder fa-2x text-blue"></i>
                                    <?php echo $Category->category; ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {



// search html data


    });

    $("#filter_search").keyup(function () {
        var filter = $(this).val();

        $("div.search_category > div > div > a").each(function () {

            // si la lista no existe el string a mostrar, ocultamos las filas
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();

                // Si coincide mostramos el tr
            } else {
                $(this).show();

            }
        });

    });

</script>