<div class="container-fluid" id="data_category" data-id_category="<?php echo $id_category; ?>" >


    <section class='content'>
        <div class="row">
            <div class="col-xl-12 col-lg-7">
                <div class="card shadow mb-4">
                    
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">

                                <form  action="/qfile/File/save" 
                                       id="formAddFile" name="formAddFile" method="POST" class="form-inline"
                                       accept-charset="utf-8" enctype="multipart/form-data">

                                    <?php echo $kerana_token; ?>
                                    <input type="hidden" name="f_id_category" value="<?php echo $id_category; ?>"/>
                                    <a href="#" class="btn btn-secondary btn-sm" title="New folder"
                                       data-target="#modalLg" 
                                       data-remote="/qfile/Category/add/<?php echo $id_category; ?>"
                                       data-toggle="modal"
                                       data-titlemodal='new folder'
                                       >
                                        <span class="icon">
                                            <i class="fas fa-folder-plus"></i>
                                        </span>
                                    </a>&nbsp;
                                    <input type="file" name="file" id="file" class="form-control form-control-sm" required>&nbsp;
                                    <button type="submit" onclick="upload()" class="btn btn-info btn-sm" title="upload new file">
                                        <i class="fa fa-upload"></i>Upload</button>
                                </form>
                            </li>
                        </ol>
                    </nav>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="row" id="data_sub_categories" >
                            <?php foreach ($Category->SubCategories AS $SubCategory): ?>

                                <div class="col-xl-2 col-md-4 mb-3">
                                    <div class="card border-left-default shadow">
                                        <div class="card-body ">
                                            <a href="/qfile/category/detail/<?php echo $SubCategory->id_category; ?>"
                                               class="text-primary">
                                                <i class="fas fa-folder fa-2x text-blue"></i>
                                                <?php echo $SubCategory->category; ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>


                            <?php endforeach; ?>


                        </div>
                        <div id="loader"></div>
                        <div class="row" id="data_files_category" >



                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
        loadFilesCategory();
    });

    function upload() {

        var form = $('#formAddFile');
        uploadFile(form);


    }





</script>