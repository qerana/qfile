<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qfile/Category/save" 
                  id="formAddCategory" name="formAddCategory" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_parent_category" id="f_parent_category" value="<?php echo $parent_category; ?>">
                <?php echo $kerana_token; ?>

                <div class='form-group form-group-sm row small'> 
                    <div class='col-sm-8'>  
                        <input type='text' id='f_category' name='f_category' 

                               class='form-control form-control-sm' required   />
                    </div>   
                </div>   
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Create</button> &nbsp;
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancel
                    </button>
                </header>
            </form>
        </div>
    </div>
</div>
<script>

// submit form
    $('#formAddCategory').submit(function (e)
    {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#modalLg').modal('hide');
                loadSubFolder($('#f_parent_category').val());
            }
        });


    });


</script>


