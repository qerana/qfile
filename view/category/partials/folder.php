<div class="container-fluid" id="data_category" data-id_category="<?php echo $id_category; ?>" >

    <section class='content' id="folder_content">
        <div class="row">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <?php foreach($Breads AS $Folder):
                        
                        if($Folder->id_category == $id_category){
                            $class = 'active';
                            $link = $Folder->category;
                        }
                        else{
                            $class = '';
                            $link = '<a href="javascript:loadSubFolder('.$Folder->id_category.')">'.$Folder->category.'</a>';
                        }
                        $active = ($Folder->id_category == $id_category) ? 'active' : '';
                        ?>
                    <li class="breadcrumb-item <?php echo $class;?>">
                       <?php echo $link;?>
                    </li>
                  <?php endforeach;?>
                </ol>
            </nav>
        </div>
        <div class="row">
            <nav aria-label="breadcrumb">
                <div class="breadcrumb">
                    <?php if ($Category->parent_category !== '0') { ?>
                        <a href="javascript:loadSubFolder(<?php echo $Category->parent_category; ?>)" class="btn btn-outline-info btn-sm" title="Atras">
                            <i class="fas fa-arrow-left"></i>
                        </a>  
                    <?php } ?>
                    &nbsp;
                    <a href="#" class="btn btn-outline-success btn-sm" title="Nueva carpeta"
                       data-target="#modalLg" 
                       data-remote="/qfile/Category/add/<?php echo $id_category; ?>"
                       data-toggle="modal"
                       data-titlemodal='nueva carpeta'
                       >
                        <span class="icon">
                            <i class="fas fa-folder-plus"></i>
                        </span>
                    </a>

                    <form  action="/qfile/File/save" 
                           id="formAddFile" name="formAddFile" method="POST" class="form-inline"
                           accept-charset="utf-8" enctype="multipart/form-data">

                        <?php echo $kerana_token; ?>
                        <input type="hidden" id="f_id_category" name="f_id_category" value="<?php echo $id_category; ?>"/>
                        &nbsp;
                        <input type="file" name="file" id="file" class="form-control form-control-sm" required>&nbsp;
                        <button type="submit" onclick="upload()" class="btn btn-success btn-sm" title="subir archivo">
                            <i class="fa fa-upload"></i>Subir </button>
                    </form>
                    </li>
                </div>
            </nav>
        </div>
        <!-- Card Body -->
        <div class="row" id="data_sub_categories" >
            <?php foreach ($Category->SubCategories AS $SubCategory): ?>

                <div class="col-xl-3 col-xs-2 col-md-3 mb-3">
                    <div class="card border-left-default shadow">
                        <div class="card-body ">
                            <a href="javascript:loadSubFolder(<?php echo $SubCategory->id_category; ?>)"
                               class="text-primary">
                                <i class="fas fa-folder fa-2x text-blue"></i>
                                <small><?php echo $SubCategory->category; ?></small>
                            </a>
                        </div>
                    </div>
                </div>


            <?php endforeach; ?>


        </div>
        <div id="loader"></div>
        <div class="row"  >
            <table class="table table-light table-borderless table-striped table-condensed">

                <tbody id="data_files_category">
                </tbody>
            </table>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
        loadFiles(<?php echo $id_category; ?>);
    });

    function upload() {

        var form = $('#formAddFile');
        uploadFile(form);

    }


    function loadSubFolder(id) {
        $('#folder_content').load("/qfile/category/loadFolder/" + id);
    }

</script>
