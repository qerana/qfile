<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qfile/Category/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_category" id="f_id_category" value="<?php echo $Category->id_category; ?>">
                <?php echo $kerana_token; ?>
                <div class='form-group form-group-sm row small'> 
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_category' name='f_category' 

                                   class='form-control form-control-sm' required   value='<?php echo $Category->category; ?>' />
                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Save</button> &nbsp;
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancel
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>

// submit form
    $('#formQerana').submit(function (e)
    {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#modalLg').modal('hide');
                location.reload();
            }
        });


    });


</script>


