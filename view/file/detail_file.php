<div class="container-fluid" >


    <section class='content'>
        <div class="row">
            <div class="col-xl-6 col-lg-5">
                <div class="card  shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h6 class="m-0 font-weight-bold text-gray-800"> 
                            <a href="/qfile/category/detail/<?php echo $File->id_category; ?>" class="btn btn-info btn-circle" title="volver">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                            <span id="detail_title"> File Detail</span>
                        </h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="top-end" >
                                <div class="dropdown-header">Options:</div>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="/qfile/File/delete/<?php echo $File->id_category.'/'.$id_file; ?>">
                                    <span class="icon">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                    <span class="text">Delete file</span>    
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 col-md-12 mb-12">
                                <div class="card shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?php echo $File->title; ?> </div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $File->size; ?> MB</div>
                                                <div class="h6 mb-0 text-gray-600"> <small><?php echo $File->mime; ?></small></div>
                                            </div>
                                            <div class="col-auto">
                                                <a href="/qfile/File/download/<?php echo $File->id_file; ?>" class="btn btn-outline-info"
                                                   target="_blank"
                                                   title="Download File">
                                                    <i class="fas fa-download fa-2x"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer bg-gray-300">
                                        in <a href="/qfile/category/detail/<?php echo $File->id_category; ?>"><?php echo $File->Category->category; ?> </a>
                                    </div>
                                     <div class="card-footer">
                                    </div>
                                    <div class="card-footer">
                                        <div class="container-fluid small" id="data_File" data-File="<?php echo $id_file; ?>">
                                            <div class="row m-1">
                                                <div class="col-sm-3 bg-gray-100 p-2">
                                                    <b>User upload</b>
                                                </div>
                                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_id_user">
                                                    <?php echo $File->User->get_fullname(); ?>
                                                </div>
                                            </div>
                                            <div class="row m-1">
                                                <div class="col-sm-3 bg-gray-100 p-2">
                                                    <b>FullPath</b>
                                                </div>
                                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_path">
                                                    <?php echo $File->path . '/' . $File->file_name;
                                                    ; ?>
                                                </div>
                                            </div>
                                            <div class="row m-1">
                                                <div class="col-sm-3 bg-gray-100 p-2">
                                                    <b>Date upload</b>
                                                </div>
                                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_date_upload">
<?php echo $File->date_upload; ?>
                                                </div>
                                            </div>
                                            <div class="row m-1">
                                                <div class="col-sm-3 bg-gray-100 p-2">
                                                    <b>Description</b>
                                                </div>
                                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_desc">
<?php echo $File->description; ?>
                                                </div>
                                            </div>
                                            <div class="row m-4">
                                                <a href="#" class="btn btn-block btn-primary"
                                                   data-target="#modalLg" 
                                                   data-remote="/qfile/File/edit/<?php echo $id_file; ?>"
                                                   data-toggle="modal"
                                                   data-titlemodal='edit File'
                                                   >
                                                    <span class="icon">
                                                        <i class="fas fa-edit"></i>
                                                    </span>
                                                    <span class="text">Edit</span>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-6 col-lg-7">

                <div class="row">
                    <div class="col-xl-12 col-md-12 mb-12">
                        <div class="card border-left-default shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Downloads</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo count($File->Downloads); ?> </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-download  fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <table class="table table-primary table-condensed">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>User</th>
                                            <th>IP</th>
                                        </tr>
                                    </thead>
                                    <tbody class="small">
<?php foreach ($File->Downloads AS $Download): ?>
                                            <tr>
                                                <td><?php echo $Download->date_download; ?></td>
                                                <td><?php echo $Download->user; ?></td>
                                                <td><?php echo $Download->ip_address; ?></td>

                                            </tr>

<?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {

    });


</script>