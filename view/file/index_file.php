<div class="container-fluid">

    <section class="content-header">
        <h5 class="text-info">
            <i class="fa fa-folder fa-2x"></i> qFiles
        </h5>
        <ol class="breadcrumb">
            <li>
                <a href="#" class="btn btn-primary btn-sm"
                   data-target="#modalLg" 
                   data-remote="/qfile/File/add"
                   data-toggle="modal"
                   data-titlemodal='Upload File'
                   >
                    <span class="icon">
                        <i class="fas fa-upload"></i>
                    </span>
                    <span class="text">Upload</span>
                </a>
            </li>
        </ol>

    </section> 



    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <input type="text" class="form-control input-sm" 
                           id="filter_search" 
                           name="filter_search"
                           aria-describedby="inputSuccess3Status"
                           value="" placeholder="search file"/>
                </div>
            </div>
            <div class="col-sm-3 float-right">
                <div class="col-xl-12 col-md-12 mb-12">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-auto">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                <span id="total"></span> files
                            </div>
                            <i class="fas fa-database fa-2x text-gray-300"></i>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="size"></div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- Card Body -->
        <div class="card-body">
            <section id='results' class='table-responsive'>
                <div id="loader"></div>
                <table class="table_search table  table-bordered table-condensed table-hover">
                    <thead class='bg-gray-600 text-white'>
                        <tr class="small">
                            <td class="bg-gray-800"></td>
                            <td>Type</td>
                            <td>Title</td>
                        </tr>
                    </thead>
                    <tbody class="small" id="list_File">
                        <!-- via json ajax -->
                    </tbody>
                </table>
            </section>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        loadFile();



// search html data


    });

    $("#filter_search").keyup(function () {
        var filter = $(this).val();

        $("table.table_search > tbody > tr").each(function () {

            // si la lista no existe el string a mostrar, ocultamos las filas
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();

                // Si coincide mostramos el tr
            } else {
                $(this).show();

            }
        });

    });

</script>