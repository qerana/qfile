<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form  action="/qfile/File/save" 
                   id="formAddFile" name="formAddFile" method="POST" class="form-horizontal"
                   accept-charset="utf-8" enctype="multipart/form-data">


                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" onclick="upload()" class="btn btn-primary btn-sm">
                        <i class="fa fa-upload"></i>Upload</button> &nbsp;
                    <button type="button" class="btn btn-dark btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancel
                    </button>
                </header>

                <?php if ($id_category !== '0') { ?>
                    <input type="hidden" name="f_id_category" value="<?php echo $id_category; ?>"/>
                <?php } else { ?>
                    <div class='form-group form-group-sm row small'> 
                        <label for='f_id_category' class='col-sm-3 col-form-label'>Category</label>  
                        <div class='col-sm-9'>  
                            <div class='input-group col-sm-8'>   
                                <select type='number' id='f_id_category' name='f_id_category' 
                                        class='form-control form-control-sm'  required>
                                    <option value="">--Category--</option>
                                    <?php foreach ($Categories AS $Category): ?>
                                        <option value="<?php echo $Category->id_category; ?>"

                                                <?php echo ($id_category == $Category->id_category) ? 'selected' : ''; ?>
                                                >
                                                    <?php echo $Category->category; ?>
                                        </option>

                                    <?php endforeach; ?>
                                </select>

                            </div>   
                        </div>   
                    </div>   
                <?php } ?>
                <div class='form-group form-group-sm row small'> 
                    <label for='file' class='col-sm-3 col-form-label'>file</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type="file" name="file" id="file" class="form-control form-control-sm" required>
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_title' class='col-sm-3 col-form-label'>Title</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_title' name='f_title' 

                                   class='form-control form-control-sm'  />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_desc' class='col-sm-3 col-form-label'>Desc</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <textarea id='f_description' name='f_description' 
                                      class='form-contro form-control-sml' cols='60' rows='5'></textarea>
                        </div>   
                    </div>   
                </div>   



            </form>
        </div>
    </div>
</div>
<script>

    function upload() {

        var form = $('#formAddFile');
        uploadFile(form);


    }


</script>


