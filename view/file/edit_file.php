<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qfile/File/modify" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_file" id="f_id_file" value="<?php echo $File->id_file; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Save</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancel
                    </button>
                </header>

                <div class='form-group form-group-sm row small'> 
                    <label for='f_title' class='col-sm-3 col-form-label'>Title</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_title' name='f_title' 

                                   class='form-control form-control-sm' required   value='<?php echo $File->title; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_description' class='col-sm-3 col-form-label'>Description</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <textarea id='f_desc' name='f_description' 
                                      class='form-contro form-control-sml' cols='60' rows='5'><?php echo $File->description; ?></textarea>
                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Save</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancel
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>


</script>


